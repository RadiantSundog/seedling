## June 29, 2023
Today, I worked on:
-queries/gardens.py
-routers/gardens.py
-queries/database.py
-main.py

I started the backend for all garden functionality.

## June 30, 2023
Today, I worked on:
-queries/gardens.py
-routers/gardens.py
-queries/database.py

I continued working on gardens backend.

## July 9, 2023
Today, I worked on:
-queries/gardens.py
-routers/gardens.py
-queries/database.py

I fixed all gardens CRUD operations.

## July 10, 2023
Today, I worked on:
-queries/gardens.py
-routers/gardens.py
-routers/journals.py
-queries/journals.py
-main.py

I fixed the database name for gardens and cleaned up my code.
I also got the journals backend set up.

## July 11, 2023
Today, I worked on:
-queries/tasks.py
-routers/tasks.py
-routers/journals.py
-queries/journals.py
-main.py

I completed all CRUD operations for plant care schedule.
I made some small changes to the code for journals.
I researched and made note of our API choices for implementation later.
I also researched how we will implement authentication on the backend and will start tomorrow.

## July 12, 2023
Today, I worked on:
-all queries
-token_auth.py

I updated the code so you can assign a plant to a specific garden by id and add a picture of the plant.
I updated formatting to match what was used in class rather than my try and see
what happens format.
I also started the update routers for plants and tasks.
I also started the last part of backend authentication.

## July 13, 2023
Today, I worked on:
-all queries
-all routers
-model.py

I updated queries functions to be more concise.
I added all models to model.py
I added commented out code for auth to be checked later.
