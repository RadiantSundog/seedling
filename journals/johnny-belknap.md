## 26 JUN 2023
Today, I worked on:

Wire frame design and sourced inspiration for gardening UI designs via Figma.

## 29 JUN 2023
Today, I worked on:

Backend/plants.py
* routers/plants.py

I started the backend for some of the plant stuff.

## 10 JUL 2023
Today, I worked on:

Backend
* queries/plants.py
* routers/plants.py
* main.py
Frontend
* MainPage.js

## 11-14 JUL 2023
This week, I worked on:

Frontend
* MainPage.js
* AccountsForms.css
* LoginForm.js
* SignUpForm.js

## 17 JUL 2023
Today, I worked on:

Frontend
* MainPage.js
* AccountsForms.css
* LoginForm.js
* SignUpForm.js

Changed the form templates again to make them modern and sleek. Added plant vector-based images to both forms. On the Main page, I made sure the SignUp and LogIn links went to the correct forms.
