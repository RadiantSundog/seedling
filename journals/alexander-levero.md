## 26 JUN 2023

Today I worked on:

- Creating a total of 15 issue tickets with Linda

We described the various functionalities we would like the App to contain, and brainstormed solutions for various design problems.

## 27 JUN 2023

Today I worked on:

- Creating initial MongoDB setup
- Created journal files

I did a code-along with my team, doing all of the initial setup for MongoDB by setting up the yml and requirements files.

## 28 JUN 2023

Today I worked on:

- Started work on authorization files

I began to add authorization functionality code to our app.

## 29 JUN 2023

Today I worked on:

- Completed and tested authorization

I was able to successfully implement authorization in our app and fully tested it, and then showed the team how the functionality and code works.

## 10 JUL 2023

Today I worked on:

- Began work on Garden list, detail, and delete pages

## 11 JUL 2023

Today I worked on:

- Choosing color palette for front-end
- Front end troubleshooting
- Implementing front-end authentication

Settled on a color palette with the team, beginning to implement it on the front-end. Helped teammates troubleshoot issues with front-end. Installed dependencies for front-end authentication with JWTdown.
